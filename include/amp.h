#ifndef AMP_H_
#define AMP_H_

#include <TComplex.h>
#include "spinor_mat.h"

TComplex Amp_s(double Eg, double costh, int hel_gamma, int mi, int mf);
TComplex Amp_t_K(double Eg, double costh, int hel_gamma, int mi, int mf);
TComplex Amp_t_Kstar(double Eg, double costh, int hel_gamma, int mi, int mf);
TComplex Amp_u(double Eg, double costh, int hel_gamma, int mi, int mf);

#endif // AMP_H_
