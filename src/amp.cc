
#include "spinor_mat.h"
#include "amp.h"

TComplex Amp_s(double Eg, double costh,  // Eg: lab, costh: cm
               int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;        // charged kaon mass
  double m4 = Mlambdastar;  // Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2() - sq(m3 + m4)) * (W.M2() - sq(m4 - m3))) /
               (2 * W.M());
  q.SetXYZM(pdk * TMath::Sqrt(1 - sq(costh)), 0, pdk * costh, m3);

  // Form Factor
  double lambda_h = 0.650;   // GeV
  double lambda_EM = 0.650;  // GeV (300 MeV)
  double sq_mom_gamma = sq(k(0)) + sq(k(1)) + sq(k(2));
  double sq_mom_kaon = sq(q(0)) + sq(q(1)) + sq(q(2));
  double F_KNLambda = (sq(lambda_h) - sq(Mkaon)) / (sq(lambda_h) + sq_mom_kaon);
  double F_gammaNN = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  // // /* lab <-- CM */
  k.Boost(W.BoostVector());
  p.Boost(W.BoostVector());
  q.Boost(W.BoostVector());
  //
  TLorentzVector pp = k + p - q;
  double s = W.M2();
  //  double t = (k - q).M2();
  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);
  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double p1[4] = {p(3), p(0), p(1), p(2)};
  //  double qq[4] = {q(3), q(0), q(1), q(2)};
  //  double tt[4] = {k(3) - q(3), k(0) - q(0), k(1) - q(1), k(2) - q(2)};

  /* amplitude calculation */
  TComplex amp = TComplex(0, 0);
  double g_KNLambdastar = 3.18;
  double kN = 1.79;
  Mat A, A1, A2;
  Mat k1_sla, p1_sla, eps_sla;
  for (int i = 0; i < 4; i++) {
    k1_sla = k1_sla + G[i] * g[i][i] * kk[i];
  }
  for (int i = 0; i < 4; i++) {
    p1_sla = p1_sla + G[i] * g[i][i] * p1[i];
  }
  for (int i = 0; i < 4; i++) {
    eps_sla = eps_sla + G[i] * g[i][i] * eps_gamma[i];
  }
  A1 = (k1_sla + p1_sla + IM * Mp) * (charge / (s - sq(Mp)));
  A2 = (k1_sla + p1_sla + IM * Mp) * k1_sla *
       (charge * kN / (2. * Mp * (s - sq(Mp))));

  amp = TComplex(0, g_KNLambdastar *
                        barDot(u(pp, mf), (A1 + A2) * eps_sla * u(p, mi)) *
                        F_KNLambda * F_gammaNN);

  return amp;
}



TComplex Amp_t_K(double Eg, double costh,  // Eg: lab, costh: cm
                 int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;        // charged kaon mass
  double m4 = Mlambdastar;  // Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2() - sq(m3 + m4)) * (W.M2() - sq(m4 - m3))) /
               (2 * W.M());
  q.SetXYZM(pdk * TMath::Sqrt(1 - sq(costh)), 0, pdk * costh, m3);
  // Form Factor
  double lambda_h = 0.650;   // GeV
  double lambda_EM = 0.650;  // GeV (300 MeV)
  double sq_mom_kaon = sq(q(0)) + sq(q(1)) + sq(q(2));
  // double sq_mom_kaon = sq(k(0) - q(0)) + sq(k(1) - q(1)) + sq(k(2) - q(2));
  double sq_mom_gamma = sq(k(0)) + sq(k(1)) + sq(k(2));
  double F_KNLambdastar =
      (sq(lambda_h) - sq(Mkaon)) / (sq(lambda_h) + sq_mom_kaon);
  double F_gammaKK = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);

  /* lab <-- CM */
  k.Boost(W.BoostVector());
  p.Boost(W.BoostVector());
  q.Boost(W.BoostVector());
  TLorentzVector pp = k + p - q;
  //  double s = W.M2();
  double t = (k - q).M2();

  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);

  /* deal with the difference of metric definition */
  //  double kk[4] = {k(3), k(0), k(1), k(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
  //  double tt[4] = {k(3) - q(3), k(0) - q(0), k(1) - q(1), k(2) - q(2)};

  /* amplitude calculation */
  TComplex amp = TComplex(0, 0);
  Mat matrixElement_ij;
  TComplex A_ij;
  // double g_KNLambdastar = 0.;
  double g_KNLambdastar = 3.18;
  TComplex A, B;
  A = 2. * charge * g_KNLambdastar;
  B = 0;
  for (int i = 0; i < 4; i++) {
    B += qq[i] * g[i][i] * eps_gamma[i];
  }

  amp =
      TComplex(0, -1. * A * (1. / (t - sq(Mkaon)) * B) *
                      barDot(u(pp, mf), u(p, mi)) * F_gammaKK * F_KNLambdastar);
  return amp;
}

TComplex Amp_t_Kstar(double Eg, double costh,  // Eg: lab, costh: cm
                     int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;                                // charged kaon mass
  double m4 = Mlambdastar;                          // Lambda(1405) mass
  TComplex Mex = TComplex(Mkstarc, -Wkstarc / 2.);  // :below Eq.(8)
  // // TComplex Mex = TComplex(Mkstarc, 0); // :below Eq.(8)
  double pdk = TMath::Sqrt((W.M2() - sq(m3 + m4)) * (W.M2() - sq(m4 - m3))) /
               (2 * W.M());
  q.SetXYZM(pdk * TMath::Sqrt(1 - sq(costh)), 0, pdk * costh, m3);

  double lambda_h = 0.650;   // GeV
  double lambda_EM = 0.650;  // GeV
  // double sq_mom_kaonstar = sq(q(0)) + sq(q(1)) + sq(q(2));
  double sq_mom_kaonstar = sq(k(0) - q(0)) + sq(k(1) - q(1)) + sq(k(2) - q(2));
  double sq_mom_gamma = sq(k(0)) + sq(k(1)) + sq(k(2));
  double F_KstarNLambdastar =
      (sq(lambda_h) - sq(Mex)) / (sq(lambda_h) + sq_mom_kaonstar);
  double F_gammaKKstar = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);

  // CM -> lab
  k.Boost(W.BoostVector());
  p.Boost(W.BoostVector());
  q.Boost(W.BoostVector());
  TLorentzVector pp = k + p - q;
  double t = (k - q).M2();

  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);
  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  double qq[4] = {q(3), q(0), q(1), q(2)};
  //  double tt[4] = {k(3) - q(3), k(0) - q(0), k(1) - q(1), k(2) - q(2)};
  /* coupling constant */
  double g_gammaKKstar = 0.254;       // GeV^-1
  double g_KstarNLambdastar = -3.18;  // from -3.18 to 3.18
  // double g_KstarNLambdastar = 0.;  // from -3.18 to 3.18
  // double g_KstarNLambdastar = - 3.18;//from -3.18 to 3.18
  /* amplitude calculation */
  TComplex amp;
  Mat B;
  double A = g_KstarNLambdastar * g_gammaKKstar;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 4; k++) {
        for (int l = 0; l < 4; l++) {
          B = B + G[l] * Levi(i, j, k, l) * kk[i] * eps_gamma[j] * qq[k];
        }
      }
    }
  }
  // B += G[0] * qq[1] * kk[2] * eps_gamma[3];
  // B -= G[0] * qq[1] * kk[3] * eps_gamma[2];
  // B -= G[0] * qq[2] * kk[1] * eps_gamma[3];
  // B += G[0] * qq[2] * kk[3] * eps_gamma[1];
  // B += G[0] * qq[3] * kk[1] * eps_gamma[2];
  // B -= G[0] * qq[3] * kk[2] * eps_gamma[1];
  // B -= G[1] * qq[0] * kk[2] * eps_gamma[3];
  // B += G[1] * qq[0] * kk[3] * eps_gamma[2];
  // B += G[1] * qq[2] * kk[0] * eps_gamma[3];
  // B -= G[1] * qq[2] * kk[3] * eps_gamma[0];
  // B -= G[1] * qq[3] * kk[0] * eps_gamma[2];
  // B += G[1] * qq[3] * kk[2] * eps_gamma[0];
  // B += G[2] * qq[0] * kk[1] * eps_gamma[3];
  // B -= G[2] * qq[0] * kk[3] * eps_gamma[1];
  // B -= G[2] * qq[1] * kk[0] * eps_gamma[3];
  // B += G[2] * qq[1] * kk[3] * eps_gamma[0];
  // B += G[2] * qq[3] * kk[0] * eps_gamma[1];
  // B -= G[2] * qq[3] * kk[1] * eps_gamma[0];
  // B -= G[3] * qq[0] * kk[1] * eps_gamma[2];
  // B += G[3] * qq[0] * kk[2] * eps_gamma[1];
  // B += G[3] * qq[1] * kk[0] * eps_gamma[2];
  // B -= G[3] * qq[1] * kk[2] * eps_gamma[0];
  // B -= G[3] * qq[2] * kk[0] * eps_gamma[1];
  // B += G[3] * qq[2] * kk[1] * eps_gamma[0];
  //
  amp = TComplex(A / (t - sq(Mex)) * barDot(u(pp, mf), G[5] * B * u(p, mi)) *
                     F_KstarNLambdastar * F_gammaKKstar,
                 0);
  return amp;
}


TComplex Amp_u(double Eg, double costh,  // Eg: lab, costh: cm
               int hel_gamma, int mi, int mf) {
  TLorentzVector k(0, 0, Eg, Eg), p(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k + p;
  /* lab --> CM */
  k.Boost(-W.BoostVector());
  p.Boost(-W.BoostVector());

  /* momentum for K+ */
  TLorentzVector q;
  double m3 = Mkaon;        // charged kaon mass
  double m4 = Mlambdastar;  // Lambda(1405) mass
  double pdk = TMath::Sqrt((W.M2() - sq(m3 + m4)) * (W.M2() - sq(m4 - m3))) /
               (2 * W.M());
  q.SetXYZM(pdk * TMath::Sqrt(1 - sq(costh)), 0, pdk * costh, m3);

  // Form Factor
  double lambda_h = 0.650;   // GeV
  double lambda_EM = 0.300;  // GeV (300 MeV)
  double sq_mom_kaon = sq(q(0)) + sq(q(1)) + sq(q(2));
  double sq_mom_gamma = sq(k(0)) + sq(k(1)) + sq(k(2));
  double F_KNLambdastar =
      (sq(lambda_h) - sq(Mkaon)) / (sq(lambda_h) + sq_mom_kaon);
  double F_gammaLL = sq(lambda_EM) / (sq(lambda_EM) + sq_mom_gamma);
  /* lab <-- CM */
  k.Boost(W.BoostVector());
  p.Boost(W.BoostVector());
  q.Boost(W.BoostVector());
  TLorentzVector pp = k + p - q;  // mom4 in lab
  //  double s = W.M2();
  //  double t = (k - q).M2();
  double U = (pp - k).M2();
  /* polarization vector */
  TComplex eps_gamma[4];
  FillPolVector(k, hel_gamma, eps_gamma);

  /* deal with the difference of metric definition */
  double kk[4] = {k(3), k(0), k(1), k(2)};
  //  double p1[4] = {p(3), p(0), p(1), p(2)};
  //  double qq[4] = {q(3), q(0), q(1), q(2)};
  double p2[4] = {pp(3), pp(0), pp(1), pp(2)};
  /* amplitude calculation */
  TComplex amp;
  Mat matrixElement_ij;
  TComplex A_ij;
  double g_KNLambdastar = 3.18;
  double kLstar = 0.4;
  double A = charge * g_KNLambdastar / (2. * Mlambdastar);
  Mat A1;
  Mat k1_sla, p2_sla, eps_sla;
  for (int i = 0; i < 4; i++) {
    k1_sla = k1_sla + G[i] * g[i][i] * kk[i];
  }
  for (int i = 0; i < 4; i++) {
    p2_sla = p2_sla + G[i] * g[i][i] * p2[i];
  }
  for (int i = 0; i < 4; i++) {
    eps_sla = eps_sla + G[i] * g[i][i] * eps_gamma[i];
  }
  A1 = eps_sla * kLstar * k1_sla * (p2_sla - k1_sla + IM * Mlambdastar) *
       (1. / (U - sq(Mlambdastar)));

  // amp =TComplex(0, -1. * A * barDot(u(pp, mf), A1 * u(p, mi)) );
  amp = TComplex(0, -1. * A * barDot(u(pp, mf), A1 * u(p, mi)) *
                        F_KNLambdastar * F_gammaLL);

  return amp;
}
