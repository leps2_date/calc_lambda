#include "spinor_mat.h"


Mat G[6]
= {
    Mat(TComplex(1, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(1, 0), TComplex(0, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(0, 0), TComplex(-1, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(-1, 0)),
    Mat(TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(1, 0),
        TComplex(0, 0), TComplex(0, 0), TComplex(1, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(-1, 0), TComplex(0, 0), TComplex(0, 0),
        TComplex(-1, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0)),
    Mat(TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, -1),
        TComplex(0, 0), TComplex(0, 0), TComplex(0, 1), TComplex(0, 0),
        TComplex(0, 0), TComplex(0, 1), TComplex(0, 0), TComplex(0, 0),
        TComplex(0, -1), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0)),
    Mat(TComplex(0, 0), TComplex(0, 0), TComplex(1, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(-1, 0),
        TComplex(-1, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(1, 0), TComplex(0, 0), TComplex(0, 0)),
    Mat(),
    Mat(TComplex(0, 0), TComplex(0, 0), TComplex(1, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(1, 0),
        TComplex(1, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0),
        TComplex(0, 0), TComplex(1, 0), TComplex(0, 0), TComplex(0, 0))
};

Mat Gamma5 // = G[5];
      = Mat(TComplex(0,0), TComplex(0,0), TComplex(1,0), TComplex(0,0),
            TComplex(0,0), TComplex(0,0), TComplex(0,0), TComplex(1,0),
            TComplex(1,0), TComplex(0,0), TComplex(0,0), TComplex(0,0),
            TComplex(0,0), TComplex(1,0), TComplex(0,0), TComplex(0,0));
// = G[0] * G[1] * G[2] * G[3] *TComplex(0,1);

//  ----------------------
//   indentify matrix
//  ----------------------
Mat IM = Mat(TComplex(1, 0), TComplex(0, 0), TComplex(0, 0), TComplex(0, 0),
             TComplex(0, 0), TComplex(1, 0), TComplex(0, 0), TComplex(0, 0),
             TComplex(0, 0), TComplex(0, 0), TComplex(1, 0), TComplex(0, 0),
             TComplex(0, 0), TComplex(0, 0), TComplex(0, 0), TComplex(1, 0));


Mat GG[6][6] = {  // G[i]*G[j]
                {IM, G[0] * G[1], G[0] * G[2], G[0] * G[3], Mat(), G[0] * G[5]},
                {G[1] * G[0], IM*(-1), G[1] * G[2], G[1] * G[3], Mat(), G[1] * G[5]},
                {G[2] * G[0], G[2] * G[1], IM*(-1), G[3] * G[3], Mat(), G[2] * G[5]},
                {G[3] * G[0], G[3] * G[1], G[3] * G[2], IM*(-1), Mat(), G[3] * G[5]},
                {Mat(), Mat(), Mat(), Mat(), Mat(), Mat()},
                {G[5] * G[0], G[5] * G[1], G[5] * G[2], G[5] * G[3], Mat(), IM}};

Mat sigmaMat[4][4]= {
                     {Mat(),  // (G[0] * G[0] - G[0] * G[0]) * TComplex(0,0.5),
                      (GG[0][1] - GG[1][0]) * TComplex(0, 0.5),
                      (GG[0][2] - GG[2][0]) * TComplex(0, 0.5),
                      (GG[0][3] - GG[3][0]) * TComplex(0, 0.5)},

                     {(GG[1][0] - GG[0][1]) * TComplex(0, 0.5),
                      Mat(),  // (G[1] * G[1] - G[1] * G[1]) * TComplex(0,0.5),
                      (GG[1][2] - GG[2][1]) * TComplex(0, 0.5),
                      (GG[1][3] - GG[3][1]) * TComplex(0, 0.5)},

                     {(GG[2][0] - GG[0][2]) * TComplex(0, 0.5),
                      (GG[2][1] - GG[1][2]) * TComplex(0, 0.5),
                      Mat(),  // (G[2] * G[2] - G[2] * G[2]) * TComplex(0,0.5),
                      (GG[2][3] - GG[3][2]) * TComplex(0, 0.5)},

                     {(GG[3][0] - GG[0][3]) * TComplex(0, 0.5),
                      (GG[3][1] - GG[1][3]) * TComplex(0, 0.5),
                      (GG[3][2] - GG[2][3]) * TComplex(0, 0.5),
                      Mat()}  //(G[3] * G[3] - G[3] * G[3]) * TComplex(0,0.5)}
};
