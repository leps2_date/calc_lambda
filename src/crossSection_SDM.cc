#include<TComplex.h>
#include "spinor_mat.h"
#include "amp.h"

double dsigma_dcosth_hioki(double Eg, double costh) {
  TComplex I;
  double sum = 0.;
  for (int mi = -1; mi <= +1; mi += 2) {
    for (int mf = -1; mf <= +1; mf += 2) {
      for (int hel_g = -1; hel_g <= +1; hel_g += 2) {
        I = TComplex(0, 0);
        I += Amp_t_K(Eg, costh, hel_g, mi, mf);
        I += Amp_t_Kstar(Eg, costh, hel_g, mi, mf);
        I += Amp_s(Eg, costh, hel_g, mi, mf);
        I += Amp_u(Eg, costh, hel_g, mi, mf);
        sum += sq(TComplex::Abs(I));
      }
    }
  }
  double m1 = 0., m2 = Mp, m3 = Mkaon, m4 = Mlambdastar;
  TLorentzVector p1(0, 0, 0, m2), k1(0, 0, Eg, Eg);
  TLorentzVector W = p1 + k1;
  double s = W.M2();
  double lambda12 = sq(s - sq(m1) - sq(m2)) - 4 * sq(m1) * sq(m2);
  double lambda34 = sq(s - sq(m3) - sq(m4)) - 4 * sq(m3) * sq(m4);
  double den = 32 * pi * s;
  return (1. / 4. * hbarc2 * sum * sqrt(lambda34 / lambda12) / den);
}

double dsigma(double Eg) {
  double n = 19.;
  double min = -1., max = 1.;
  double dcosth = (max - min) / n;
  double sigma = 0;
  for (int i = 0; i < n; i++) {
    double x1 = min + dcosth * i;
    double x2 = min + dcosth * (i + 1.);
    double f1 = dsigma_dcosth_hioki(Eg, x1);
    double f2 = dsigma_dcosth_hioki(Eg, x2);
    sigma += (f1 + f2) * dcosth / 2.;
  }

  return sigma;
}
